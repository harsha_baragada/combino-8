package com.combino.repository;

import com.combino.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Serializable> {
}
