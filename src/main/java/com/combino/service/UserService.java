package com.combino.service;

import com.combino.entities.UserEntity;
import com.combino.repository.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public int userSignup(UserEntity userEntity){
        UserEntity savedUser = userRepository.save(userEntity);
        int response =0;

        if(savedUser != null){
            response =1;
        }
        return response;
    }
}
