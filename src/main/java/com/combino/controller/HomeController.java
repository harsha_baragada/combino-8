package com.combino.controller;


import com.combino.entities.UserEntity;
import com.combino.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class HomeController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/giveMeMessage")
    public String getMessage() {
        return "Hello!! How are you?";
    }

    @PostMapping(value = "/userSignup")
    public int signup(@RequestBody UserEntity user) {
        return userService.userSignup(user);
    }
}
